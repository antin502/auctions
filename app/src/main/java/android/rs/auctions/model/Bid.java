package android.rs.auctions.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by vlada on 14.4.17..
 */
@DatabaseTable(tableName = "bids")
public class Bid {
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField
    public double price;
    @DatabaseField
    public Date dateTime;
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    public Auction auction;
    @DatabaseField(canBeNull = true, foreign = true, foreignAutoRefresh = true)
    public User user;

    public Bid() {}

    public long getId() {return id;}

    public Bid setId(long id) {this.id = id;return this;}

    public double getPrice() {return price;}

    public Bid setPrice(double price) {this.price = price;return this;}

    public Date getDateTime() {return dateTime;}

    public Bid setDateTime(Date dateTime) {this.dateTime = dateTime;return this;}

    public Auction getAuction() {return auction;}

    public Bid setAuction(Auction auction) {this.auction = auction;return this;}

    public User getUser() {return user;}

    public Bid setUser(User user) {this.user = user;return this;}
}
