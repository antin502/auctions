package android.rs.auctions.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by vlada on 14.4.17..
 */
@DatabaseTable(tableName = "users")
public class User {
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField
    private String name;
    @DatabaseField
    private String email;
    @DatabaseField
    private String password;
    @DatabaseField
    private String picture;
    @DatabaseField
    private String address;
    @DatabaseField
    private String phone;
    private List<Auction> auctions;
    private List<Bid> bids;

    public User(){}

    public long getId() {return id;}

    public User setId(long id) {this.id = id;return this;}

    public String getName() {return name;}

    public User setName(String name) {this.name = name;return this;}

    public String getEmail() {return email;}

    public User setEmail(String email) {this.email = email;return this;}

    public String getPassword() {return password;}

    public User setPassword(String password) {this.password = password;return  this;}

    public String getPicture() {return picture;}

    public User setPicture(String picture) {this.picture = picture;return  this;}

    public String getAddress() {return address;}

    public User setAddress(String address) {this.address = address;return  this;}

    public String getPhone() {return phone;}

    public User setPhone(String phone) {this.phone = phone;return  this;}

    public List<Auction> getAuctions() {return auctions;}

    public User setAuctions(List<Auction> auctions) {this.auctions = auctions;return  this;}

    public List<Bid> getBids() {return bids;}

    public User setBids(List<Bid> bids) {this.bids = bids;return  this;}
}
