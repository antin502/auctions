package android.rs.auctions.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vlada on 14.4.17..
 */
@DatabaseTable(tableName = "items")
public class Item {
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField
    public String name;
    @DatabaseField
    public String description;
    @DatabaseField
    public String picture;
    @DatabaseField
    public boolean sold;
    public List<Auction> auctions = new ArrayList<>();

    public Item() {}

    public long getId() {return id;}

    public Item setId(long id) {this.id = id;return this;}

    public String getName() {return name;}

    public Item setName(String name) {this.name = name;return this;}

    public String getDescription() {return description;}

    public Item setDescription(String description) {this.description = description;return this;}

    public String getPicture() {return picture;}

    public Item setPicture(String picture) {this.picture = picture;return this;}

    public boolean isSold() {return sold;}

    public Item setSold(boolean sold) {this.sold = sold;return this;}

    public List<Auction> getAuctions() {return auctions;}

    public Item setAuctions(List<Auction> auctions) {this.auctions = auctions;return this;}

}
