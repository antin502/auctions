package android.rs.auctions.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by vlada on 14.4.17..
 */
@DatabaseTable(tableName = "auctions")
public class Auction {
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField
    public double startPrice;
    @DatabaseField
    public Date startDate;
    @DatabaseField
    public Date endDate;
    @DatabaseField(canBeNull = true, foreign = true, foreignAutoRefresh = true)
    public User user;
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    public Item item;
    public List<Bid> bids = new ArrayList<>();

    public Auction() {}

    public long getId() {return id;}

    public Auction setId(long id) {this.id = id;return this;}

    public double getStartPrice() {return startPrice;}

    public Auction setStartPrice(double startPrice) {this.startPrice = startPrice;return this;}

    public Date getStartDate() {return startDate;}

    public Auction setStartDate(Date startDate) {this.startDate = startDate;return this;}

    public Date getEndDate() {return endDate;}

    public Auction setEndDate(Date endDate) {this.endDate = endDate;return this;}

    public User getUser() {return user;}

    public Auction setUser(User user) {this.user = user;return this;}

    public Item getItem() {return item;}

    public Auction setItem(Item item) {this.item = item;return this;}

    public List<Bid> getBids() {return bids;}

    public Auction setBids(List<Bid> bids) {this.bids = bids;return this;}
}
