package android.rs.auctions.activities;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.rs.auctions.R;
import android.rs.auctions.adapter.auctionsAdapter.AuctionTabAdapter;
import android.rs.auctions.model.Auction;
import android.rs.auctions.model.Bid;
import android.rs.auctions.notification.BidReceiver;
import android.rs.auctions.notification.BidService;
import android.rs.auctions.tools.Data;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.Date;

public class AuctionActivity extends AppCompatActivity {


    private String input_bid ="";

    private BidReceiver bidReceiver;
    private AlarmManager manager;
    public static String ADD_BID = "ADD_BID";
    private PendingIntent pendingIntent;
    private Auction auction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auction);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar appBar = getSupportActionBar();
        appBar.setDisplayHomeAsUpEnabled(true);

        int id = getIntent().getIntExtra("id",0);
        auction = Data.instance.auctions.get(id);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.auction_tabs);
        final FloatingActionButton addBid = (FloatingActionButton) findViewById(R.id.addBid);

        tabLayout.addTab(tabLayout.newTab().setText("Info"));
        tabLayout.addTab(tabLayout.newTab().setText("Bids"));
        tabLayout.setTabGravity(TabLayout.MODE_SCROLLABLE);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.auction_pager);
        AuctionTabAdapter adapter = new AuctionTabAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==0){
                    addBid.hide();
                }else{
                    addBid.show();
                }
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        addBid.setOnClickListener(addBid());
        setBidReceiver();
    }

    private void setBidReceiver() {
        bidReceiver = new BidReceiver();
        Intent intent = new Intent(this, BidService.class);
        pendingIntent = PendingIntent.getService(this,0,intent,0);

        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public View.OnClickListener addBid(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AuctionActivity.this);
                builder.setTitle("Your bid");
                final EditText bid_value = new EditText(AuctionActivity.this);
                bid_value.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                builder.setView(bid_value);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        input_bid = String.valueOf(bid_value.getText());
                        //kreiranje novog bida
                        //dodavanje u item
                        RuntimeExceptionDao<Bid, Long> bidDao= Data.getHelper(AuctionActivity.this).getBidDataDao();
                        long numbBids = bidDao.countOf();
                        Float num = Float.parseFloat(input_bid);
                        Bid newBid = new Bid().setPrice(num).setDateTime(new Date()).setId(numbBids+1);
                        newBid.setAuction(auction);
                        bidDao.create(newBid);

                        boolean najveca = true;
                        for (Bid bid : auction.getBids()){
                            if(bid.getPrice()>num){
                                najveca=false;
                                break;
                            }
                        }
                        if(najveca){
                            //prikazi notifikaciju
                            bidReceiver.setBid(newBid);
                            manager.setRepeating(AlarmManager.RTC_WAKEUP,
                                    System.currentTimeMillis(), 0, pendingIntent);
                            IntentFilter filter = new IntentFilter();
                            filter.addAction(ADD_BID);
                            registerReceiver(bidReceiver,filter);
                        }
                        auction.getBids().add(newBid);
                        Toast.makeText(AuctionActivity.this, "Your bid of "+ input_bid+" has been sent", Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        };
    }
}
