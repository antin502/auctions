package android.rs.auctions.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.rs.auctions.MainActivity;
import android.rs.auctions.R;
import android.rs.auctions.model.Auction;
import android.rs.auctions.model.Bid;
import android.rs.auctions.model.Item;
import android.rs.auctions.model.User;
import android.rs.auctions.tools.Data;
import android.rs.auctions.tools.orm_lite.DatabaseHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.sql.SQLException;
import java.util.List;

public class SplashScreenActivity extends AppCompatActivity {

    private String SPLASH_SCREEN_TIME = "splash_screen_time";
    private String SHOW_SPLASH_SCREEN = "show_splash_screen";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        RuntimeExceptionDao<Item, Long> itemDao = Data.getHelper(this).getItemDataDao();
        RuntimeExceptionDao<Auction, Long> auctionDao = Data.getHelper(this).getAuctionDataDao();
        RuntimeExceptionDao<Bid, Long> bidDao = Data.getHelper(this).getBidDataDao();
        RuntimeExceptionDao<User, Long> userDao = Data.getHelper(this).getUserDataDao();

        Data.items = itemDao.queryForAll();
        for (Item item : Data.items){
            try {
                List<Auction> auctions = auctionDao.queryBuilder().where().eq("item_id",item.getId()).query();
                item.setAuctions(auctions);
            } catch (SQLException e) {
                Toast.makeText(this,"Problem dobavljanja aukcija od itema : "+String.valueOf(item.getId()),Toast.LENGTH_SHORT).show();
            }
        }

        Data.auctions = auctionDao.queryForAll();
        for (Auction auction : Data.auctions){
            try {
                List<Bid> bids = bidDao.queryBuilder().where().eq("auction_id",auction.getId()).query();
                auction.setBids(bids);
            } catch (SQLException e) {
                Toast.makeText(this,"Problem dobavljanja ponuda od aukcije : "+String.valueOf(auction.getId()),Toast.LENGTH_SHORT).show();
            }
        }

        Data.me = userDao.queryForId((long) 1);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        boolean splashEnable = prefs.getBoolean(SHOW_SPLASH_SCREEN,true);

        if(splashEnable){
            int splashTime = Integer.parseInt(prefs.getString(SPLASH_SCREEN_TIME, "3"));
            new Handler().postDelayed(new Runnable(){
                @Override
                public void run() {
                    startMainActivity();
                }
            }, splashTime*1000);
        }else{
            startMainActivity();
        }

        Button imgBtn = (Button) findViewById(R.id.splashScreenImage);
        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMainActivity();
            }
        });

    }

    private void startMainActivity(){
        Intent mainIntent = new Intent(SplashScreenActivity.this,MainActivity.class);
        startActivity(mainIntent);
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
