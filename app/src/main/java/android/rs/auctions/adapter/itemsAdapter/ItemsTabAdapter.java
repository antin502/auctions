package android.rs.auctions.adapter.itemsAdapter;

import android.rs.auctions.fragmets.itemsFragments.TabAuctionsItemFragment;
import android.rs.auctions.fragmets.itemsFragments.TabInfoItemFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by vlada on 20.4.17..
 */

public class ItemsTabAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public ItemsTabAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabInfoItemFragment info = new TabInfoItemFragment();
                return info;
            case 1:
                TabAuctionsItemFragment auctions = new TabAuctionsItemFragment();
                return auctions;
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}
