package android.rs.auctions.adapter.auctionsAdapter;

import android.app.Activity;
import android.rs.auctions.R;
import android.rs.auctions.model.Auction;
import android.rs.auctions.model.Bid;
import android.rs.auctions.tools.Data;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by vladimir_antin on 25.5.17..
 */

public class BidsAdapter extends BaseAdapter {
    private Activity activity;

    private Auction auction;
    public BidsAdapter(Activity activity) {
        this.activity = activity;
        int id = activity.getIntent().getIntExtra("id",0);
        auction = Data.instance.auctions.get(id);
    }


    @Override
    public int getCount() {
        return auction.getBids().size();
    }

    @Override
    public Object getItem(int position) {
        return auction.getBids().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        Bid bid = auction.getBids().get(position);

        if (convertView == null)
            vi = activity.getLayoutInflater().inflate(R.layout.bids_item, null);

        TextView name = (TextView) vi.findViewById(R.id.bid_name);
        TextView description = (TextView) vi.findViewById(R.id.bid_date);
        TextView price = (TextView) vi.findViewById(R.id.bid_price);
        ImageView image = (ImageView) vi.findViewById(R.id.bid_icon);

        name.setText(bid.getAuction().getItem().getName());
        description.setText(String.valueOf(bid.getDateTime()));
        price.setText(String.valueOf(bid.getPrice()));

        image.setImageResource(R.drawable.auction);

        return vi;
    }
}