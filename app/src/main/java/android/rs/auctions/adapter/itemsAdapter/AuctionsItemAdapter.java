package android.rs.auctions.adapter.itemsAdapter;

import android.app.Activity;
import android.rs.auctions.R;
import android.rs.auctions.model.Auction;
import android.rs.auctions.model.Item;
import android.rs.auctions.tools.Data;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Created by vlada on 14.4.17..
 */

public class AuctionsItemAdapter extends BaseAdapter {
    private Activity activity;
    private Item item;

    public AuctionsItemAdapter(Activity activity){
        this.activity = activity;
        int id = activity.getIntent().getIntExtra("id",0);
        this.item = Data.items.get(id);
    }



    @Override
    public int getCount() {return item.getAuctions().size();}

    @Override
    public Object getItem(int position) {
        return item.getAuctions().get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        Auction auction = item.getAuctions().get(position);

        if(convertView==null)
            vi = activity.getLayoutInflater().inflate(R.layout.auction_list, null);

        TextView name = (TextView)vi.findViewById(R.id.auctionName);
        TextView description = (TextView)vi.findViewById(R.id.auctionDescription);
        TextView price = (TextView)vi.findViewById(R.id.auctionPrice);
        ImageView image = (ImageView)vi.findViewById(R.id.item_auction_icon);

        name.setText(auction.getItem().getName());
        description.setText(auction.getItem().getDescription());
        price.setText(String.valueOf(auction.getStartPrice()));

        image.setImageResource(R.drawable.auction);

        return  vi;
    }
}
