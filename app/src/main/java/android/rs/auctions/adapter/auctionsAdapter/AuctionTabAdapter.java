package android.rs.auctions.adapter.auctionsAdapter;

import android.rs.auctions.fragmets.auctionsFragment.TabBidsAuctionFragment;
import android.rs.auctions.fragmets.auctionsFragment.TabInfoAuctionFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by vladimir_antin on 25.5.17..
 */

public class AuctionTabAdapter  extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public AuctionTabAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabInfoAuctionFragment info = new TabInfoAuctionFragment();
                return info;
            case 1:
                TabBidsAuctionFragment bids = new TabBidsAuctionFragment();
                return bids;
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
