package android.rs.auctions.adapter;

import android.app.Activity;
import android.rs.auctions.R;
import android.rs.auctions.model.Item;
import android.rs.auctions.tools.Data;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Created by vlada on 14.4.17..
 */

public class ItemsAdapter extends BaseAdapter {
    private Activity activity;

    public ItemsAdapter(Activity activity){this.activity = activity;}



    @Override
    public int getCount() {return Data.items.size();}

    @Override
    public Object getItem(int position) {
        return Data.items.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        Item item = Data.instance.items.get(position);

        if(convertView==null)
            vi = activity.getLayoutInflater().inflate(R.layout.item_list, null);

        TextView name = (TextView)vi.findViewById(R.id.name);
        TextView description = (TextView)vi.findViewById(R.id.description);
        ImageView image = (ImageView)vi.findViewById(R.id.item_icon);

        name.setText(item.getName());
        description.setText(item.getDescription());

        image.setImageResource(R.drawable.items);


        return  vi;
    }
}
