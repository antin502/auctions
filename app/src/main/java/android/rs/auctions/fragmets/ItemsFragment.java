package android.rs.auctions.fragmets;

import android.content.Intent;
import android.os.Bundle;
import android.rs.auctions.activities.ItemActivity;
import android.rs.auctions.adapter.ItemsAdapter;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

/**
 * Created by vlada on 17.4.17..
 */

public class ItemsFragment extends ListFragment {

    public static ItemsFragment newInstance() {

        ItemsFragment IF = new ItemsFragment();

        return IF;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(getActivity(), ItemActivity.class);
        intent.putExtra("id", position);
        startActivity(intent);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ItemsAdapter adapter = new ItemsAdapter(getActivity());
        setListAdapter(adapter);
    }



}
