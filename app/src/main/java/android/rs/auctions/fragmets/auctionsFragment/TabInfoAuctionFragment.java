package android.rs.auctions.fragmets.auctionsFragment;

import android.os.Bundle;
import android.rs.auctions.R;
import android.rs.auctions.model.Auction;
import android.rs.auctions.model.Item;
import android.rs.auctions.tools.Data;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by vladimir_antin on 25.5.17..
 */

public class TabInfoAuctionFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.auction_info_fragment,container,false);
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        int id = getActivity().getIntent().getIntExtra("id",0);
        Auction auction = Data.instance.auctions.get(id);
        TextView txtName = (TextView) getActivity().findViewById(R.id.txtaItemName);
        txtName.setText(auction.getItem().getName());

        TextView txtDescr = (TextView) getActivity().findViewById(R.id.txtaItemDescr);
        txtDescr.setText(auction.getItem().getDescription());

        TextView txtStart = (TextView) getActivity().findViewById(R.id.txtAuctionStartDate);
        txtStart.setText(String.valueOf(auction.getStartDate()));

        TextView txtEnd = (TextView) getActivity().findViewById(R.id.txtAuctionEndDate);
        txtEnd.setText(String.valueOf(auction.getEndDate()));

        TextView txtPrice = (TextView) getActivity().findViewById(R.id.txtAuctionPrice);
        txtPrice.setText(String.valueOf(auction.getStartPrice()));
    }
}
