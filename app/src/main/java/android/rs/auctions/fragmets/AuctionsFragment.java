package android.rs.auctions.fragmets;

import android.content.Intent;
import android.os.Bundle;
import android.rs.auctions.activities.AuctionActivity;
import android.rs.auctions.adapter.AuctionsAdapter;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

/**
 * Created by vlada on 17.4.17..
 */
public class AuctionsFragment extends ListFragment {

    public static AuctionsFragment newInstance() {

        AuctionsFragment af = new AuctionsFragment();

        return af;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(getActivity(), AuctionActivity.class);
        intent.putExtra("id",position);
        startActivity(intent);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AuctionsAdapter adapter = new AuctionsAdapter(getActivity());
        setListAdapter(adapter);
    }
}
