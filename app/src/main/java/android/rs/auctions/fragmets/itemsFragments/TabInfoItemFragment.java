package android.rs.auctions.fragmets.itemsFragments;

import android.os.Bundle;
import android.rs.auctions.R;
import android.rs.auctions.model.Item;
import android.rs.auctions.tools.Data;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Created by vlada on 20.4.17..
 */

public class TabInfoItemFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.item_info_fragment,container,false);
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Item item = Data.items.get(getActivity().getIntent().getIntExtra("id",0));
        TextView txtName = (TextView) getActivity().findViewById(R.id.txtItemName);
        TextView txtDescr = (TextView) getActivity().findViewById(R.id.txtItemDescr);
        txtName.setText(item.getName());
        txtDescr.setText(item.getDescription());

        ImageView image = (ImageView) getActivity().findViewById(R.id.IconItem);
        image.setImageResource(R.drawable.auction);
    }
}
