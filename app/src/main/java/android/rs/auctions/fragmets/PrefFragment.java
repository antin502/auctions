package android.rs.auctions.fragmets;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.rs.auctions.R;
import android.rs.auctions.tools.Data;
import android.widget.Toast;

/**
 * Created by vlada on 2.5.17..
 */

public class PrefFragment extends PreferenceFragment{
    SharedPreferences sharedPreferences;
    private String SPLASH_SCREEN_TIME = "splash_screen_time";
    private String USER_NAME = "user_name";
    private String USER_EMAIL = "user_email";
    private String USER_PASSWORD = "user_password";
    private String USER_ADDRESS = "user_address";
    private String USER_PHONE = "user_phone";

    public static PrefFragment newInstance() {

        PrefFragment pf = new PrefFragment();

        return pf;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        init();

            sharedPreferences.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                    EditTextPreference pref = null;
                    String value=null;
                    if(!key.equals("show_splash_screen")){
                        pref = (EditTextPreference) findPreference(key);
                        value = sharedPreferences.getString(key,"");
                        if (key.equals(SPLASH_SCREEN_TIME)) {
                            pref.setSummary(value);
                        }else if(key.equals(USER_NAME)){
                            Data.me.setName(value);
                        }else if(key.equals(USER_EMAIL)){
                            Data.me.setEmail(value);
                        }else if(key.equals(USER_PASSWORD)){
                            Data.me.setPassword(value);
                        }else if(key.equals(USER_ADDRESS)){
                            Data.me.setAddress(value);
                        }else if(key.equals(USER_PHONE)){
                            Data.me.setPassword(value);
                        }
                        Data.getHelper(getActivity()).getUserDataDao().update(Data.me);
                        Data.me = Data.getHelper(getActivity()).getUserDataDao().queryForId((long) 1);
                        init();
                    }

                }
            });
    }

    private void init(){
        setValue(SPLASH_SCREEN_TIME,"3","3");
        setValue(USER_NAME, Data.me.getName(),Data.me.getName());
        setValue(USER_EMAIL,Data.me.getEmail(),Data.me.getEmail());
        setValue(USER_PHONE,Data.me.getPhone(),Data.me.getPhone());
        setValue(USER_ADDRESS,Data.me.getAddress(),Data.me.getAddress());
        setValue(USER_PASSWORD,Data.me.getPassword(),"********");
    }

    private void setValue(String key, String valueText,String valueSummery){
        EditTextPreference sett = (EditTextPreference) findPreference(key);
        sett.setSummary(sharedPreferences.getString(key,valueSummery));
        sett.setText(sharedPreferences.getString(key,valueText));
    }

}
