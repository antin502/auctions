package android.rs.auctions.fragmets.auctionsFragment;

import android.content.Intent;
import android.os.Bundle;
import android.rs.auctions.activities.AuctionActivity;
import android.rs.auctions.adapter.AuctionsAdapter;
import android.rs.auctions.adapter.auctionsAdapter.BidsAdapter;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

/**
 * Created by vlada on 20.4.17..
 */

public class TabBidsAuctionFragment extends ListFragment{


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        BidsAdapter adapter = new BidsAdapter(getActivity());
        setListAdapter(adapter);
    }


}
