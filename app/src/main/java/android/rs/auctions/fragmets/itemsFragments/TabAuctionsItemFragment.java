package android.rs.auctions.fragmets.itemsFragments;

import android.content.Intent;
import android.os.Bundle;
import android.rs.auctions.activities.AuctionActivity;
import android.rs.auctions.adapter.AuctionsAdapter;
import android.rs.auctions.adapter.itemsAdapter.AuctionsItemAdapter;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

/**
 * Created by vlada on 20.4.17..
 */

public class TabAuctionsItemFragment extends ListFragment{


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
//        Intent intent = new Intent(getActivity(), AuctionActivity.class);
//        intent.putExtra("id",postion);
//        startActivity(intent);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AuctionsItemAdapter adapter = new AuctionsItemAdapter(getActivity());
        setListAdapter(adapter);
    }


}
