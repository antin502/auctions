package android.rs.auctions;

import android.rs.auctions.fragmets.AuctionsFragment;
import android.rs.auctions.fragmets.ItemsFragment;
import android.rs.auctions.fragmets.PrefFragment;
import android.rs.auctions.tools.Data;
import android.rs.auctions.tools.FragmentTransition;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;


public class MainActivity extends AppCompatActivity {

    private ActionBarDrawerToggle mDrawerToggle;
    private RelativeLayout mDrawerPane;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    protected static final int NAV_DRAWER_ITEM_INVALID = -1;

    private DrawerLayout drawerLayout;
    private Toolbar actionBarToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = mDrawerTitle = getTitle();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        getActionBarToolbar();
        if (savedInstanceState == null) {
            onNavigationItemClicked(Data.positionDrawer);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setupNavDrawer();
    }


    private void setupNavDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        if (drawerLayout == null) {
            // current activity does not have a drawer.
            return;
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setItemIconTintList(null); //colors icon in navbar
            setupDrawerSelectListener(navigationView);
            setSelectedItem(navigationView);
        }

    }

    private void setSelectedItem(NavigationView navigationView) {
//        int selectedItem = getSelfNavDrawerItem();
        navigationView.setCheckedItem(Data.positionDrawer);
    }

    private void setupDrawerSelectListener(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        drawerLayout.closeDrawers();
                        onNavigationItemClicked(menuItem.getItemId());
                        return true;
                    }
                });
    }

    private void onNavigationItemClicked(final int itemId) {
        if(itemId == getSelfNavDrawerItem()) {
            // Already selected
            closeDrawer();
            return;
        }

        goToNavDrawerItem(itemId);
    }

    private void goToNavDrawerItem(int item) {
        switch (item) {
            case R.id.nav_items:
                Data.positionDrawer=R.id.nav_items;
                FragmentTransition.to(ItemsFragment.newInstance(), this);
                break;
            case R.id.nav_auctions:
                Data.positionDrawer = R.id.nav_auctions;
                FragmentTransition.to(AuctionsFragment.newInstance(), this);
                break;
            case R.id.nav_settings:
                Data.positionDrawer = R.id.nav_settings;
                FragmentTransition.toPref(PrefFragment.newInstance(),this);
                break;
        }
    }


    protected ActionBar getActionBarToolbar() {
        if (actionBarToolbar == null) {
            actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
            if (actionBarToolbar != null) {
                setSupportActionBar(actionBarToolbar);
                final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setIcon(R.drawable.auction);
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
                actionBar.setHomeButtonEnabled(true);
                mDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,actionBarToolbar, R.string.app_name, R.string.app_name) {
                    public void onDrawerClosed(View view) {
                        getSupportActionBar().setTitle(mTitle);
                        invalidateOptionsMenu();
                    }

                    public void onDrawerOpened(View drawerView) {
                        getSupportActionBar().setTitle(mTitle);
                        invalidateOptionsMenu();
                    }
                };
            }
        }
        return getSupportActionBar();
    }


    protected int getSelfNavDrawerItem() {
        return NAV_DRAWER_ITEM_INVALID;
    }

    protected void openDrawer() {
        if(drawerLayout == null)
            return;

        drawerLayout.openDrawer(GravityCompat.START);
    }

    protected void closeDrawer() {
        if(drawerLayout == null)
            return;

        drawerLayout.closeDrawer(GravityCompat.START);
    }

}
