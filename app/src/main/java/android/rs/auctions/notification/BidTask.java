package android.rs.auctions.notification;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.rs.auctions.MainActivity;
import android.rs.auctions.activities.AuctionActivity;
import android.rs.auctions.activities.ItemActivity;

/**
 * Created by vladimir_antin on 12.5.17..
 */

public class BidTask extends AsyncTask<Void, Void, Void> {
    private Context context;


    public static String RESULT_CODE = "RESULT_CODE";

    public BidTask(Context context)
    {
        this.context = context;
    }

    @Override
    protected void onPreExecute()
    {
        //postaviti parametre, pre pokretanja zadatka ako je potrebno
    }


    @Override
    protected Void doInBackground(Void... params) {

        //simulacija posla koji se obavlja u pozadini i traje duze vreme
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        return null;
    }


    @Override
    protected void onPostExecute(Void result) {

        Intent ints = new Intent(AuctionActivity.ADD_BID);
        context.sendBroadcast(ints);
    }
}

