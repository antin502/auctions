package android.rs.auctions.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.rs.auctions.R;
import android.rs.auctions.activities.ItemActivity;
import android.rs.auctions.model.Bid;
import android.rs.auctions.tools.Data;
import android.support.v4.app.NotificationCompat;

/**
 * Created by vladimir_antin on 12.5.17..
 */

public class BidReceiver extends BroadcastReceiver {

    private int notificationID =1;
    private Bid bid;
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager mNotificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(R.drawable.items);
        mBuilder.setContentTitle("Wow Your bid is great");

        intent = new Intent(context, ItemActivity.class);
        long id = bid.getAuction().getItem().getId();
        intent.putExtra("id", (int)Data.getItemPosition(id));
        PendingIntent pendingIntent = PendingIntent.getActivity(context,notificationID+1,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.addAction(new NotificationCompat.Action(R.drawable.items,"Open Item "+id,pendingIntent));


        mBuilder.setContentText("id is: "+String.valueOf(bid.getId())+" and price: "+String.valueOf(bid.getPrice()));
        mNotificationManager.notify(notificationID, mBuilder.build());
    }

    public void setBid(Bid bid){
        this.bid = bid;
    }
}
