package android.rs.auctions.tools;

import android.app.Activity;
import android.rs.auctions.R;
import android.rs.auctions.model.Auction;
import android.rs.auctions.model.Bid;
import android.rs.auctions.model.Item;
import android.rs.auctions.model.User;
import android.rs.auctions.tools.orm_lite.DatabaseHelper;

import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by vlada on 14.4.17..
 */

public class Data {

    public static List<Item> items = new ArrayList<>();
    public static List<Bid> bids = new ArrayList<>();
    public static List<Auction> auctions = new ArrayList<>();
    public static User me;


    public static int positionDrawer = R.id.nav_items;
    public static Data instance = new Data();

    private  Data(){
    }


    public static DatabaseHelper getHelper(Activity activity) {
            return DatabaseHelper.getHelper(activity);
    }

    public static long getItemPosition(long id){
        for (Item item :items) {
            if(item.getId()==id){
                return items.indexOf(item);
            }
        }
        return 0;
    }
}
