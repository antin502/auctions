package android.rs.auctions.tools.orm_lite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.rs.auctions.R;
import android.rs.auctions.model.Auction;
import android.rs.auctions.model.Bid;
import android.rs.auctions.model.Item;
import android.rs.auctions.model.User;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by vladimir_antin on 18.5.17..
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "auctions.db";
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;

    // the DAO object we use to access the SimpleData table
    private RuntimeExceptionDao<Item, Long> itemRuntimeDao= null;
    private RuntimeExceptionDao<Auction, Long> auctionRuntimeDao = null;
    private RuntimeExceptionDao<User, Long> userRuntimeDao = null;
    private RuntimeExceptionDao<Bid, Long> bidRuntimeDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    private static DatabaseHelper helper = null;

    public static synchronized DatabaseHelper getHelper(Context context) {
        if (helper == null) {
            helper = new DatabaseHelper(context);
        }
        return helper;
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Item.class);
            TableUtils.createTable(connectionSource, Auction.class);
            TableUtils.createTable(connectionSource, Bid.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        itemRuntimeDao = getItemDataDao();
        userRuntimeDao = getUserDataDao();
        auctionRuntimeDao= getAuctionDataDao();
        bidRuntimeDao = getBidDataDao();
        initData();
        /*https://github.com/j256/ormlite-examples/blob/master/android/HelloAndroid/src/com/example/helloandroid/DatabaseHelper.java*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Item.class, true);
            TableUtils.dropTable(connectionSource, Auction.class, true);
            TableUtils.dropTable(connectionSource, Bid.class, true);
            TableUtils.dropTable(connectionSource, User.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public RuntimeExceptionDao<Item, Long> getItemDataDao() {
        if (itemRuntimeDao == null) {
            itemRuntimeDao = getRuntimeExceptionDao(Item.class);
        }
        return itemRuntimeDao;
    }
    public RuntimeExceptionDao<Auction, Long> getAuctionDataDao() {
        if (auctionRuntimeDao == null) {
            auctionRuntimeDao = getRuntimeExceptionDao(Auction.class);
        }
        return auctionRuntimeDao;
    }
    public RuntimeExceptionDao<User, Long> getUserDataDao() {
        if (userRuntimeDao == null) {
            userRuntimeDao = getRuntimeExceptionDao(User.class);
        }
        return userRuntimeDao;
    }
    public RuntimeExceptionDao<Bid, Long> getBidDataDao() {
        if (bidRuntimeDao == null) {
            bidRuntimeDao = getRuntimeExceptionDao(Bid.class);
        }
        return bidRuntimeDao;
    }

    @Override
    public void close() {
        super.close();
        itemRuntimeDao=null;
        auctionRuntimeDao=null;
        userRuntimeDao=null;
        bidRuntimeDao=null;
    }

    private void initData(){
        long auctionsId = 1;
        long bidsId = 1;

        for (long i=1;i!=10;i++){
            Item item = new Item().setId(i).setName("item "+i).setSold(false).setDescription("description "+i);
            itemRuntimeDao.create(item);
            for (long j=0;j!=3;j++){
                Auction auction = new Auction().setId(auctionsId).setStartPrice(500*(auctionsId+1))
                        .setStartDate(new Date()).setEndDate(new Date()).setItem(item);
                item.getAuctions().add(auction);
                auctionRuntimeDao.create(auction);
                auctionsId++;
                for (long k=0;k!=5;k++){
                    Bid bid = new Bid().setId(bidsId).setAuction(auction).setDateTime(new Date()).setPrice(500*(bidsId+1));
                    auction.getBids().add(bid);
                    bidRuntimeDao.create(bid);
                    bidsId++;
                }
                auctionRuntimeDao.update(auction);
            }
            itemRuntimeDao.update(item);
        }
        userRuntimeDao.create(new User()
                .setId(1)
                .setPassword("mypass")
                .setEmail("myemail@ggg.com")
                .setPhone("06406")
                .setAddress("myaddr")
                .setName("new name"));
    }

}
