package android.rs.auctions.tools;

import android.rs.auctions.R;
import android.rs.auctions.fragmets.PrefFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;


public class FragmentTransition
{
	public static void to(Fragment newFragment, FragmentActivity activity)
	{
		popBackStack(activity);
		FragmentTransaction transaction = activity.getSupportFragmentManager()
			.beginTransaction()
			.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
			.replace(R.id.mainContent, newFragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}

	public static void toPref(PrefFragment prefFragment, FragmentActivity activity){
		popBackStack(activity);
		android.app.FragmentTransaction transaction = activity.getFragmentManager()
				.beginTransaction()
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
				.replace(R.id.mainContent,prefFragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}

	public static void popBackStack(FragmentActivity activity)
	{
		activity.getFragmentManager().popBackStack();
		activity.getSupportFragmentManager().popBackStack();
	}
}
